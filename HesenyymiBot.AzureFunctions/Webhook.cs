using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace HesenyymiBot.AzureFunctions
{
    public static class Webhook
    {
        [FunctionName("Webhook")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "Webhook/{token}")] Update update, string token, ILogger log)
        {
            log.LogInformation($"Webhook update {update.Id}");

            var botToken = Environment.GetEnvironmentVariable("BotToken") ?? throw new ArgumentNullException("BotToken undefined!");
            var webhookToken = Environment.GetEnvironmentVariable("WebhookToken") ?? throw new ArgumentNullException("WebhookToken undefined!");

            if (token != webhookToken)
                return new UnauthorizedResult();

            var telegramClient = new TelegramBotClient(botToken);

            if (update.Type == UpdateType.ChatMember)
            {
                if (update.ChatMember != null &&
                    update.ChatMember.Chat?.Id != null &&
                    update.ChatMember.NewChatMember.Status == ChatMemberStatus.Left &&
                    (update.ChatMember.OldChatMember.Status == ChatMemberStatus.Member || update.ChatMember.OldChatMember.Status == ChatMemberStatus.Restricted))
                {
                    var user = update.ChatMember.NewChatMember.User;
                    var username = $"{user.FirstName}{(String.IsNullOrEmpty(user.LastName) ? "" : " " + user.LastName)}";
                    var message = username + " left the group";
                    var entities = new List<MessageEntity> {
                        new MessageEntity {
                            Type = MessageEntityType.TextMention,
                            Offset = 0,
                            Length = username.Length,
                            User = user
                        },
                        new MessageEntity {
                            Type = MessageEntityType.Italic,
                            Offset = 0,
                            Length = message.Length
                        }
                    };

                    await telegramClient.SendTextMessageAsync(update.ChatMember.Chat.Id, message, entities: entities, disableNotification: true);
                }
            }

            log.LogInformation($"Webhook update done");

            return new OkResult();
        }
    }
}
