using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace HesenyymiBot.AzureFunctions
{
    public static class SetupWebhook
    {
        [FunctionName("SetupWebhook")]
        public static async Task RunAsync([TimerTrigger("0 30 5 * * *")] TimerInfo timer, ILogger log)
        {
            log.LogInformation("Setting up webhook");

            var botToken = Environment.GetEnvironmentVariable("BotToken") ?? throw new ArgumentNullException("BotToken undefined!");
            var webhookToken = Environment.GetEnvironmentVariable("WebhookToken") ?? throw new ArgumentNullException("WebhookToken undefined!");
            var webhookAddress = Environment.GetEnvironmentVariable("WebhookAddress") ?? throw new ArgumentNullException("WebhookAddress undefined!");
            if (!webhookAddress.EndsWith('/'))
                webhookAddress += "/";

            var telegramClient = new TelegramBotClient(botToken);
            var telegramSelf = await telegramClient.GetMeAsync();
            if (telegramSelf == null)
                throw new NullReferenceException("Could not get bot self!");

            var baseUri = new Uri(webhookAddress, UriKind.Absolute);
            var webhookUri = new Uri(baseUri, webhookToken);

            var allowedUpdates = new List<UpdateType> { UpdateType.ChatMember };
            await telegramClient.SetWebhookAsync(webhookUri.AbsoluteUri, allowedUpdates: allowedUpdates);

            log.LogInformation("Done setting up webhook");
        }
    }
}
